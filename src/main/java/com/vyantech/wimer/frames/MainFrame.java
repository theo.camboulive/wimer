/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.vyantech.wimer.listeners.AbstractWindowListener;
import com.vyantech.wimer.stopwatch.StopWatch;
import com.vyantech.wimer.stopwatch.StopWatchListener;
import com.vyantech.wimer.tick.TickListener;
import com.vyantech.wimer.tick.TickProvider;
import com.vyantech.wimer.tray.TrayManager;
import com.vyantech.wimer.tray.TrayManagerImpl;

public class MainFrame extends JFrame implements StopWatchListener, TickListener {
	
	private static final int LAYOUT_GAP = 10;
	private static final long serialVersionUID = -3250465181368972859L;

	private final JLabel timeLabel = new JLabel("", SwingConstants.CENTER);
	private final JButton startButton = new JButton();
	private final JButton resetButton = new JButton("RESET");

	private final StopWatch watch = StopWatch.createUnstarted();
	private final TickProvider tickProvider = new TickProvider();

	@SuppressWarnings("unused")
	private final TrayManager trayManager;

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300, 300);
		setResizable(false);

		addWindowListener(new AbstractWindowListener() {
			@Override
			public void windowClosing(WindowEvent e) {
				tickProvider.stop();
			}
		});
		
		setupElements();
		watch.addStatusListener(this);
		tickProvider.addListener(this);
		
		trayManager = new TrayManagerImpl(watch);
	}

	@Override
	public void reset() {
		showDuration();
		updateButton();
		resetButton.setEnabled(false);
	}

	@Override
	public void started() {
		showDuration();
		updateButton();
		resetButton.setEnabled(true);
	}

	@Override
	public void stopped() {
		showDuration();
		updateButton();
		resetButton.setEnabled(true);
	}
	
	@Override
	public void tick() {
		showDuration();
	}

	private void setupElements() {
		setLayout(new BorderLayout(LAYOUT_GAP, LAYOUT_GAP));
		
		showDuration();
		updateButton();
		setWhiteFontColor(timeLabel);

		Container container = getContentPane();
		container.setBackground(new Color(49, 60, 75));
		container.add(timeLabel, BorderLayout.CENTER);
		addButtons(container);
	}

	private void showDuration() {
		timeLabel.setText(watch.elapsedDisplayString());
	}
	
	private void toggleStartStop(ActionEvent e) {
		if (watch.isRunning()) {
			watch.stop();
		} else {
			watch.start();
		}
	}

	private void updateButton() {
		startButton.setText(getPlayText());
	}

	private static void setButtonUI(JButton button) {
		button.setFont(new Font("Tahoma", Font.BOLD, 14));
		button.setForeground(new Color(33, 150, 243));
		// button.setForeground(Color.WHITE);
		// button.setBackground(new Color(33, 150, 243));
		// button.setOpaque(true);
		button.setBorderPainted(false);
	}

	private static void setWhiteFontColor(JComponent label) {
		label.setFont(new Font("Tahoma", Font.PLAIN, 32));
		label.setForeground(Color.WHITE);
	}

	private void addButtons(Container container) {
		FlowLayout btnLayout = new FlowLayout(LAYOUT_GAP);
		Container btnContainer = new Container();
		btnContainer.setLayout(btnLayout);
		container.add(btnContainer, BorderLayout.PAGE_END);

		setButtonUI(startButton);
		btnContainer.add(startButton);

		setButtonUI(resetButton);
		btnContainer.add(resetButton);

		startButton.addActionListener(this::toggleStartStop);
		resetButton.addActionListener(e -> {
			watch.reset();
		});
	}

	private String getPlayText() {
		return watch.isRunning() ? "STOP" : "START";
	}
}
