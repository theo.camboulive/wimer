/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.stopwatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class StopWatchImpl implements StopWatch {
	private boolean running = false;
	private long start = 0;
	private long elapsed = 0;
	private final List<StopWatchListener> listeners = new ArrayList<>();

	@Override
	public void addStatusListener(StopWatchListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeStatusListener(StopWatchListener listener) {
		listeners.remove(listener);
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void stop() {
		if (running) {
			elapsed += System.currentTimeMillis() - start;
			start = 0;
			running = false;
			listeners.forEach(StopWatchListener::stopped);
		}
	}

	@Override
	public void reset() {
		stop();
		start = elapsed = 0;
		listeners.forEach(StopWatchListener::reset);
	}

	public void start() {
		if (!running) {
			start = System.currentTimeMillis();
			running = true;
			listeners.forEach(StopWatchListener::started);
		}
	}

	@Override
	public long elapsed(TimeUnit unit) {
		long ms = elapsed + (running ? (System.currentTimeMillis() - start) : 0);
		return unit.convert(ms, TimeUnit.MILLISECONDS);
	}

	@Override
	public String elapsedDisplayString() {
		final long duration = elapsed(TimeUnit.SECONDS);
		long h = (duration / 3600);
		long m = (duration % 3600) / 60;
		long s = (duration % 60);
		return String.format("%02dh %02dm %02ds", h, m, s);
	}

}
