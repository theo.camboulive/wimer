/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.stopwatch;

public interface StopWatchListener {
	public void stopped();
	public void started();
	public void reset();
}
