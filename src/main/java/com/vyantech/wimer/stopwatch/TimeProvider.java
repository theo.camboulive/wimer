/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.stopwatch;

import java.util.concurrent.TimeUnit;

public interface TimeProvider {
	public long elapsed(TimeUnit unit);

	public String elapsedDisplayString();
}
