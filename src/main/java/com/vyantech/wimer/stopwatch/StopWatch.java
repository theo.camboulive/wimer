/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.stopwatch;

public interface StopWatch extends TimeProvider {

	public static StopWatch createUnstarted() {
		return new StopWatchImpl();
	}
	
	public void addStatusListener(StopWatchListener listener);

	public void removeStatusListener(StopWatchListener listener);

	public boolean isRunning();

	public void stop();

	public void reset();

	public void start();

}
