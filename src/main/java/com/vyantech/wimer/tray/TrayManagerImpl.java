/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.tray;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.vyantech.wimer.listeners.AbstractMounseListener;
import com.vyantech.wimer.stopwatch.StopWatch;
import com.vyantech.wimer.stopwatch.StopWatchListener;

public class TrayManagerImpl extends AbstractMounseListener implements TrayManager, StopWatchListener {
	private final StopWatch watch;
	private final MenuItem timeMenu;
	private final MenuItem pauseItem;

	public TrayManagerImpl(StopWatch watch) {
		SystemTray tray = SystemTray.getSystemTray();
		Image image = null;
		try {
			image = ImageIO.read(getClass().getResource("/assets/w2.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		PopupMenu popup = new PopupMenu();
		timeMenu = new MenuItem();
		timeMenu.setEnabled(false);
		popup.add(timeMenu);

		pauseItem = new MenuItem();
		popup.add(pauseItem);
		pauseItem.addActionListener(this::toggleStartStop);

		TrayIcon trayIcon = new TrayIcon(image);
		trayIcon.setPopupMenu(popup);
		trayIcon.setImageAutoSize(true);
		trayIcon.addMouseListener(this);

		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			e.printStackTrace();
		}

		this.watch = watch;
		updateMenuLabels();
		watch.addStatusListener(this);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		updateMenuLabels();
	}

	@Override
	public void stopped() {
		updateMenuLabels();
	}

	@Override
	public void started() {
		updateMenuLabels();
	}

	@Override
	public void reset() {
		updateMenuLabels();
	}

	private void updateMenuLabels() {
		timeMenu.setLabel(watch.elapsedDisplayString());
		pauseItem.setLabel(getPlayText());
	}

	private void toggleStartStop(ActionEvent e) {
		if (watch.isRunning()) {
			watch.stop();
		} else {
			watch.start();
		}
	}

	private String getPlayText() {
		return watch.isRunning() ? "STOP" : "START";
	}

}
