/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.tick;

import java.util.ArrayList;
import java.util.List;

public class TickProvider {
	private long intervalMs;
	private boolean run = true;

	private final List<TickListener> listeners = new ArrayList<>();

	public TickProvider() {
		this(1000);
	}

	public TickProvider(long intervalMs) {
		this.intervalMs = intervalMs;
		new Thread(this::run).start();
	}

	public void addListener(TickListener listener) {
		listeners.add(listener);
	}

	public void removeListener(TickListener listener) {
		listeners.remove(listener);
	}

	private void run() {
		while (run) {
			listeners.forEach(TickListener::tick);
			
			try {
				Thread.sleep(intervalMs);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		run = false;
	}
}
