/**
 * (C) king.com Ltd 2020
 */
package com.vyantech.wimer.tick;

public interface TickListener {
	public void tick();
}
